#!/bin/sh

# Restarts an openbet application using ob_control.
#
# The script requires an app argument, and takes optional box and database arguments.
# If the app is not running as an appserver, then ot_cache can't be used therefore simple process matching
# will be used.
# Script has been taken from openbet_cron_restart.sh and correctrd to use ob_monitor except obmonitor in Pre-Prod. Git - https://git.nonprod.williamhill.plc/kpryadkin/bash_for_services_restart
#
# Example usage (appserv):
# sh ob_control_services_restart.sh -b gib -a oxi_rep -m Y >> /opt/openbet/log/OXi/oxireplication/control_gib.log 2>&1
#
# Example usage (non appserv):
# sh ob_control_services_restart.sh -a oxi_db_sync -c dbSync_brsux156.cfg -m N >> /opt/openbet/log/OXi/oxirepserver/clients/dbSync/control.log 2>&1
#
#
# This script will be called from a cron job,
# hence we set up all the environment stuff here
#

# set environment variables to equal what ob_control would have
. "$HOME/.bashrc"

OBCONTROL=""
APP=""

while getopts b:a:d:m:c: o
do
	case $o in
		a) APP=$OPTARG;;
		m) ISAPP=$OPTARG;;
		c) APPCFG=$OPTARG;;
	esac
done

help()
{
	cat << __END__
usage: $0
		-a <application to restart>
		-b <suffix to append to ob_control_>
		-m <running as an appserver or not? (default Y) Y|N>
		-c <configuration file for the app - if required>
__END__
	exit 2
}

if [ "$APP" = "" ]; then
	help
fi

OB="ob_control"

if [ "$ISAPP" = "" ]; then
	ISAPP="Y"
fi

if [ "$APPCFG" = "" -a "$ISAPP" != "Y" ]; then
	echo "ERROR : If -m N, then a config file is required"
	help
fi

# Count number of apps of this name known to ob_control - Oxi uses different amount of processes in obmonitor - caused error
APPS_FOUND=`$OB check $APP | grep app | wc -l `

if [ "$APPS_FOUND" -eq 0 ]; then
	echo "ERROR: Application '$APP' unknown to ob_control"
	echo "Aborting"
	help
fi

echo "`date` ======== SCHEDULED APP RESTART ====================="

# Control statements are universal for both appserver and non appserver

control_stop='$OB stop $APP'
control_start='$OB start $APP'

# Figure out which command to use to search for the app depending on the mode it's running
# (either appserver or not)
if [ "$ISAPP" = "Y" ]; then
	proc_search="$OB check $APP | grep 'OK' | wc -l"
	dead_app_search="$OB check $APP | grep 'DOWN' | wc -l"
else
	proc_search="ps -ef | grep $APPCFG | grep -v grep | grep -v openbet_cron_restart.sh | wc -l"
fi

# Keep track of the original number of processess running
original_proc_num=`eval $proc_search`

echo "`date` - there are $original_proc_num app processes running. Stop."

# Stopping the app
eval $control_stop
sleep 1

# Check number of processes
if [ "$ISAPP" != "Y" ]; then

# Non appserver behavior - just search the processes
	num_procs=`eval $proc_search`
	while [ "$num_procs" -gt 1 ]; do
		num_procs=`eval $proc_search`
		echo "`date` - Waiting for process to shut down ... ($num_procs/$original_proc_num) still running "
		sleep 1
	done

fi

echo "`date` - App stopped ..."
echo "`date` - Checking zombie processes and kill it ..."

# Check zombie process and kill it
CURPID=$$
ppid=$(ps axo pid=,stat= | awk '$2~/^Z/ { print }')
if [ -z $ppid ] ; then
	echo No zomdie process found.
else
	for ppid in $(ps axo pid=,stat= | awk '$2~/^Z/ { print $1 }'); do
		arraycounter=1
		while true; do
			FORLOOP=FALSE

		# Get all the child process id
		  for i in `ps -ef| awk '$3 == '$ppid' { print $2 }'`; do
				if [ $i -ne $CURPID ]; then
					procid[$arraycounter]=$i
					arraycounter=`expr $arraycounter + 1`
					ppid=$i
					FORLOOP=TRUE
					printf "Detected process : %d\n from process %d\n : User %d\n Successfully Killed" "i" "getpid()" "getuser()"
				else
					printf "Detected process : %d\n from process %d\n : User %d\n could not kill" "i" "getpid()" "getuser()"
				fi
			done

			if [ "$FORLOOP" = "FALSE" ]; then
				arraycounter=`expr $arraycounter - 1`
			# We want to kill child process id first and then parent id's
				while [ "$arraycounter" -ne "0" ]; do
					kill -9 "${procid[$arraycounter]}" >/dev/null
					arraycounter=`expr $arraycounter - 1`
				done
			fi

		done

		# Kill Parent ID
		kill -9 $CURPID

	done
fi
echo "`date` - Restarting the app..."

# Check shared memory segments to clean up

for s in $(ipcs -m | awk '$6~/^0/ { print $1 }' | cut -f1 -d' ' | xargs); do
	ipcrm -M $s;
	echo "`date` - checked and cleaned up not used Shared Memory Segments"
done

# Starting the app
started=0
while [ "$started" -lt 1 ]; do
  sleep 1
	eval $control_start

# Make sure we restarted correctly
	num_procs=`eval $proc_search`
	itr=0

	while [[ "$num_procs" -lt "$original_proc_num" && "$itr" -lt 60 ]]; do
		echo "`date` - Waiting for app to start up ... ($num_procs/$original_proc_num)"
		num_procs=`eval $proc_search`
		sleep 1
		itr=`expr $itr + 1`
	done

	if [ "$num_procs" -ge "$original_proc_num" ]; then
		started=1
	else
		echo "`date` - App did not start up, attempting to start again..."
	fi

done

echo "`date` ======== APP RESTARTED - END  ====================="
